var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var parseurl = require('parseurl');
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var fs = require('fs');


var conn = mysql.createConnection({
    host: '127.0.0.1',
    user: 'root',
    password: '@Loverudal2637',
    database: 'kakao'
});

conn.connect();


var app = express();

app.set('views', './views');
app.set('view engine', 'jade');
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use('/static', express.static(__dirname + '/public'));

app.use(session({
    secret: '1234DSFs@adf1234!@#$asd',
    resave: false,
    saveUninitialized: true,
    store: new MySQLStore({
        host: '127.0.0.1',
        port: 3306,
        user: 'root',
        password: '@Loverudal2637',
        database: 'kakao'
    })
}));



//로그인화면


app.post('/login', function(req, res) {
    var requestUserId = req.body.userId;
    var requestPassword = req.body.password;
    var requestKoreanName = req.body.koreanName;
    var loginSql = "SELECT * FROM users where userId='" + requestUserId + "'";
    conn.query(loginSql, function(err, dbResultList, fields) {
        if (err) {
            console.log("DB Error Occurred");
            return;
        }

        var dbUserId = dbResultList[0].userId;
        var dbUserPassword = dbResultList[0].password;
        var dbId = dbResultList[0].id;
        var dbKoreanName = dbResultList[0].koreanName;

        var loginSuccess = (requestUserId == dbUserId && requestPassword == dbUserPassword);

        if (loginSuccess) {
            req.session.userId = dbId;
            console.log(dbKoreanName);
            res.redirect('/main');
        } else {
            console.log("Not Login");
            res.send('Not Login <a href="/main">메인페이지로 갑니다.</a>');
        }
    });
});



app.get('/login', function(req, res) {
    res.render('kakaoLogin');
});


//로그인 후 메인화면
app.get('/main', function(req, res) {
    var currentUserId = req.session.userId;
    var userSql = 'SELECT * FROM users WHERE id=?'
    conn.query(userSql, currentUserId, function(err, user) {
        var mainSql = "SELECT users.*, friends.userId, friends.friendsKoreanName  FROM users INNER JOIN friends ON users.id=? and friends.userId=?;"
        conn.query(mainSql, [req.session.userId, user[0].userId], function(err, row) {
            if (err) {
                console.log("DB Error");
                return;
            } else {
                console.log(user[0])
                res.render('kakaoMain', {
                    row: row[0],
                    rows: row,
                    dbKoreanName: user[0].koreanName
                })
            }
        })
    })
});




//친구찾기

app.get(['/search'], function(req, res) {
    var dbKoreanName = req.query.koreanName;
    var searchSql = "SELECT * FROM users WHERE koreanName like '%" + dbKoreanName + "%'";
    conn.query(searchSql, function(err, row, fields) {
        if (err) {
            console.log("DB Error Occurred");
            return;
        } else {
            console.log(row[0].koreanName)
            res.render('kakaoSearch', {
                row: row[0]
            });
        };
    });
});


//친구추가----미완성
//sql 부분에 내 userId와 상대방 koreanName이 들어가야함
//search부분에서는 상대방 koreanName이 query로 들어가는데 그걸 post로 넣는 방법을 모르겠음
//이게 콜백 지옥인지 확인해보기

app.post('/kakaoFriendsAdd/:userId', function(req, res) {
    var userId = req.params.userId;
    var friendsSql = 'SELECT * FROM users WHERE userId=?';
    conn.query(friendsSql, [req.params.userId], function(err, friends) {

        if (friends) {
            var myUserId = req.session.userId;
            var userSql = 'SELECT * FROM users WHERE id=?'
            conn.query(userSql, myUserId, function(err, user) {

                if (user) {
                    console.log(user)
                }

                console.log(userId)
                console.log(friends[0].koreanName)
                var searchSql = 'INSERT INTO friends(userId, friendsKoreanName) VALUES(?,?)';
                conn.query(searchSql, [user[0].userId, friends[0].koreanName], function(err, row) {

                    if (err) {
                        console.log("DB Error Occurred");
                    } else {
                        console.log(row);
                        res.redirect('/main');
                    }
                });
            })
        }
    })
})


app.get('/kakaoFriendsAdd/:userId', function(req, res) {
    var userId = req.params.userId;
    var sql = 'SELECT * FROM users WHERE userId=?';
    conn.query(sql, [req.params.userId], function(err, row) {
        if (err) {
            console.log(err);
            res.status(500).send('Internal Server Error');
        } else {
            console.log(row[0].koreanName)
            res.render('kakaoFriendsAdd', {
                row: row[0]
            });
        }
    });
});




//회원가입화면
app.post('/register', function(req, res) {
    var user = {
        userId: req.body.userId,
        password: req.body.password,
        koreanName: req.body.koreanName,
        ssnFront: req.body.ssnFront,
        phoneNumber: req.body.phoneNumber
    };

    var sql = 'INSERT INTO users SET ?';
    conn.query(sql, user, function(err, results, fields) {
        if (err) {
            console.log(err);
            res.status(500);
            //에러가 있었음을 알림
        } else {
            res.redirect('/login');
        }

    });
});


app.get('/register', function(req, res) {
    res.render('kakaoRegister');
});




//자유게시판    chat   chat.jade

app.get('/chat', function(req, res) {
    var forSelectListSql = "SELECT * FROM board";

    conn.query(forSelectListSql, function(err, rows) {
        if (err) {
            console.log("DB Error Occurred");
        } else {
            var theDay = rows[0].modiDate.getUTCFullYear() + " . " + (parseInt(rows[0].modiDate.getMonth()) + parseInt(1)) + "." + rows[0].modiDate.getDate()
            console.log(theDay)
            res.render('kakaoChat', {
                rows: rows,
                theDay: theDay
            });
        }
    });
});


//자유게시판 글쓰기 chatWrite   chatWrite.jade

app.post('/chat/write', function(req, res) {
    var boardDM = {
        displayName: req.body.displayName,
        title: req.body.title,
        content: req.body.content,
        password: req.body.password
    };

    var insertBoardSql = "INSERT INTO board SET ?";
    conn.query(insertBoardSql, boardDM, function(err, rows) {
        if (err) {
            console.log("DB Error Occurred");
        } else {
            res.redirect('/chat');
        }
    });
});



app.get('/chat/write', function(req, res, next) {
    res.render('kakaoChatWrite');
});


//자유게시판 chatRead   chatRead.jade 조회 및 수정

app.get('/chat/read/:boardId', function(req, res) {
    var boardId = req.params.boardId;
    var sql = "SELECT * FROM board where boardId=?";
    conn.query(sql, [boardId], function(err, row) {

        if (err) {
            console.log("DB Error Occurred");
        } else {
            var theDay = row[0].modiDate.getUTCFullYear() + " . " + (parseInt(row[0].modiDate.getMonth()) + parseInt(1)) + "." + row[0].modiDate.getDate()
            var a = row[0];
            console.log(a)
            res.render('kakaoChatRead', {
                post: a,
                theDay: theDay
            });
        }
    });
});


//자유게시판 chatModify   chatModify.jade 수정

app.post('/chat/modify/:boardId', function(req, res, next) {
    var boardId = req.params.boardId;
    var displayName = req.body.displayName;
    var title = req.body.title;
    var content = req.body.content;
    var password = req.body.password;

    var sql = "UPDATE board SET displayName=?, title=?, content=?, regDate=now() where boardId=? and password=?";
    conn.query(sql, [displayName, title, content, boardId, password], function(err, row) {
        if (err) {
            console.error("DB Error Occurred")
        } else {
            console.log(row.regDate)
            res.redirect('/chat');
        }
    });
});


app.get('/chat/modify/:boardId', function(req, res, next) {
    var boardId = req.params.boardId;
    var sql = "SELECT * FROM board WHERE boardId=?";
    conn.query(sql, [boardId], function(err, row) {
        if (err) {
            console.log("DB Error Occurred");
        } else {
            res.render('kakaoChatModify', {
                row: row[0]
            });
        }
    });
});


//자유게시판 chatDelete   chatDelete.jade
app.post('/chat/delete/:boardId', function(req, res, next) {
    var boardId = req.params.boardId;
    var sql = "DELETE FROM board WHERE boardId=?";
    conn.query(sql, [boardId], function(err, row) {
        console.log(boardId)
        res.redirect('/chat');
    });
});




app.get('/chat/delete/:boardId', function(req, res, next) {
    var boardId = req.params.boardId;
    var sql = "SELECT * FROM board WHERE boardId=?";
    conn.query(sql, [boardId], function(err, row) {
        if (err) {
            console.log("DB Error Occurred");
        } else {
            res.render('kakaoChatDelete', {
                row: row[0]
            });
        }
    });
});




//서버 연결

app.listen(3001, function() {
    console.log('3001 OPEN');
});




//REST로 게시판 기능 넣기 ( + 에 글 추가넣기 )
// 각 아이디마다 사진 다르게 해보기
//개인정보 수정할 수 있도록 해보기